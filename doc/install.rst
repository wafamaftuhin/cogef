.. _download_and_install:

============
Installation
============

.. toctree::
    :hidden:

    platforms/platforms

Requirements
============

* Python_ 3.7
* NumPy_ 1.16
* ASE_ 3.18

.. _Python: http://www.python.org/
.. _ASE: http://wiki.fysik.dtu.dk/ase
.. _NumPy: http://docs.scipy.org/doc/numpy/reference/


Installation
============

* Installation with pip::

  $ sudo pip install --upgrade pip
  $ # If you want to install ase with pip and the latest ase pypi release is
  $ # still equal to 3.17, install the newest ase version with the following
  $ # command
  $ pip install git+https://gitlab.com/ase/ase.git
  $ # In all cases you can install cogef with the next command
  $ pip install ase-cogef

* Developer installation with git (when there is no need for merge requests)

  Clone the repository (you need ssh keys)::

    $ git clone git@gitlab.com:cogef/cogef.git

  or if you do not have and do not want to create ssh keys, use::

    $ git clone https://gitlab.com/cogef/cogef.git

  In any case, add ``cogef`` folder to the $PYTHONPATH environment variable.
  Add ``cogef/bin`` folder to the $PATH environment variable.

* Developer installation with git (when you may want to create merge requests)

  Go to https://gitlab.com/cogef/cogef and fork the project, then clone it
  with your gitlab account name (you need ssh keys)::

    $ git clone git@gitlab.com:your-user-name/cogef.git

  Add ``cogef`` folder to the $PYTHONPATH environment variable.
  Add ``cogef/bin`` folder to the $PATH environment variable.


Testing
========

Please run the tests::

  $ cogef test
